from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from murr_notifications.models import FeedNotification, SubscribeNotification


class NotificationsViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]

    @action(detail=True, methods=['DELETE'])
    def read_murr_card(self, request, pk):
        FeedNotification.objects.get(murr_card__pk=pk, to_murren=request.user).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['DELETE'])
    def read_subscribe_notify(self, request, pk):
        SubscribeNotification.objects.get(pk=pk, to_murren=request.user).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
