from murr_game.models import Player


async def check_winner_group(murr_game_room_data):
    murren_name_in_first_group = set(([murren['murren_player_data']['murren_name'] for murren in
                                       murr_game_room_data['first_group']]))
    murren_name_in_second_group = set([murren['murren_player_data']['murren_name'] for murren in
                                       murr_game_room_data['second_group']])
    defeated_members_in_first_group = set(
        murr_game_room_data['defeated_murren_name_by_group'].get('first_group', []))
    defeated_members_in_second_group = set(
        murr_game_room_data['defeated_murren_name_by_group'].get('second_group', []))
    if murren_name_in_first_group.issubset(defeated_members_in_first_group):
        for murren_name in murren_name_in_second_group:
            m = Player.objects.filter(murren_player__username=murren_name).first()
            m.increase_exp(5)
        murr_game_room_data['winner_group_data'] = {'winner_group': 'second_group',
                                                    'winners_name': list(murren_name_in_second_group)}
    if murren_name_in_second_group.issubset(defeated_members_in_second_group):
        for murren_name in murren_name_in_first_group:
            m = Player.objects.filter(murren_player__username=murren_name).first()
            m.increase_exp(5)
        murr_game_room_data['winner_group_data'] = {'winner_group': 'first_group',
                                                    'winners_name': list(murren_name_in_first_group)}
