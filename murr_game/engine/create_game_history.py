from murr_game.models import MurrBattleRoomStats, MurrGameRoom, Player


async def create_game_history(room_data: dict) -> None:
    """
    Сохранение информации о бое
    """
    union_list_players = (*room_data['first_group'], *room_data['second_group'])
    game_history = MurrBattleRoomStats.objects.create()
    all_players = [Player.objects.get(murren_player__username=player['murren_player_data']['murren_name'])
                   for player in union_list_players]
    winners = [Player.objects.get(murren_player__username=player)
               for player in room_data['winner_group_data']['winners_name']]

    game_history.players.add(*all_players)
    game_history.winners.add(*winners)
