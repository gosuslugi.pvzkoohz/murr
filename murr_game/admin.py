from django.contrib import admin

from murr_game.models import Player, Skills, FirstGroup, SecondGroup, MurrGameRoom

admin.site.register(Skills)
admin.site.register(Player)
admin.site.register(FirstGroup)
admin.site.register(SecondGroup)
admin.site.register(MurrGameRoom)
