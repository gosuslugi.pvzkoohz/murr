from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import PlayerViewSet, PlayerLeaderBoardViewSet


urlpatterns = [
    path('players_leader_board', PlayerLeaderBoardViewSet.as_view())
]

router = DefaultRouter()
router.register('', PlayerViewSet, basename='player_data')

urlpatterns += router.urls
