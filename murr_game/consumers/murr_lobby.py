import uuid

from channels.db import database_sync_to_async

from murr_game.models import FirstGroup, SecondGroup, MurrGameRoom
from murr_websocket.consumers.base import BaseMurrWebSocketConsumer
from murren.serializers import MurrenSerializer


class MurrGameLobbyConsumer(BaseMurrWebSocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope['murr_ws_type'] = 'murr_battle_room'

    async def connect(self):
        await super().connect()
        await self.channel_layer.group_add(self.scope['murr_ws_name'], self.channel_name)
        murr_battle_rooms_available = await self.get_murr_battle_rooms_available()
        for i, room in enumerate(murr_battle_rooms_available):
            murrens_in_battle_room = await self.get_murr_game_room_data(room['id'])
            murr_battle_rooms_available[i]['room_groups'] = murrens_in_battle_room['room_groups']
            murr_battle_rooms_available[i]['room_id'] = room['id']
        await self._group_send(murr_battle_rooms_available, gan='murrenConnectToBattleLobby')

    @database_sync_to_async
    def get_murr_battle_rooms_available(self):
        return list(MurrGameRoom.objects.filter(is_available=True).values())

    async def disconnect(self, code):
        murren_disconnected = {
            'destroyed_murr_battle_room_ids': await self.destroy_created_murr_battle_room(self.scope['murren']),
            'murren_id': self.scope['murren'].id
        }
        await self._group_send(murren_disconnected, gan='disconnect_MurrGameLobbyConsumer')
        await self.leave_all_room_on_disconnect(self.scope['murren'])

        await self.channel_layer.group_discard(self.scope['murr_ws_name'], self.channel_name)
        await super().disconnect(code=code)

    @database_sync_to_async
    def destroy_created_murr_battle_room(self, murren):

        destroyed_murr_battle_room_ids = []

        for i in MurrGameRoom.objects.filter(room_leader=murren):
            destroyed_murr_battle_room_ids.append(i.id)
            i.delete()
        return destroyed_murr_battle_room_ids

    async def gan__create_murr_battle_room(self, gan):
        room_type = gan['data'].get('murr_battle_room_type')
        if not room_type:
            return await self._trow_error({'detail': 'Missing murr_battle_room_type'}, gan=gan['gan'])

        room_name = f"murr_game_room_{str(uuid.uuid4())[0:4]}_by_{self.scope['murren'].username}"
        murr_game_room = await self.create_murr_game_room(room_name, self.scope['murren'], room_type)

        data = await self.get_murr_game_room_data(murr_game_room.id)

        await self._group_send(data, gan=gan['gan'])

    async def gan__join_in_room(self, gan):
        room_id = gan['data'].get('room_id')
        if not room_id:
            return await self._trow_error({'detail': 'Missing room_id'}, gan=gan['gan'])
        room_group = gan['data'].get('room_group')
        if not room_group:
            return await self._trow_error({'detail': 'Missing room_group'}, gan=gan['gan'])
        await self.add_murren_to_room_group(self.scope['murren'], room_id, room_group)
        murr_game_room_data = await self.get_murr_game_room_data(room_id)
        await self._group_send(murr_game_room_data, gan=gan['gan'])

    @database_sync_to_async
    def add_murren_to_room_group(self, murren_instance, room_id, room_group):
        Group = None
        if room_group == 'firstGroup':
            Group = FirstGroup
        if room_group == 'secondGroup':
            Group = SecondGroup
        Group.objects.get_or_create(room_instance_id=room_id, murren_player=murren_instance)

    @database_sync_to_async
    def leave_all_room_on_disconnect(self, murren_instance):
        FirstGroup.objects.filter(murren_player=murren_instance).delete()
        SecondGroup.objects.filter(murren_player=murren_instance).delete()

    @database_sync_to_async
    def create_murr_game_room(self, room_name, murren_player, room_type):
        group_len_template = {
            'one_vs_one': 1,
            'two_vs_two': 2,
        }
        murr_game_room = MurrGameRoom.objects.create(room_name=room_name, room_leader=murren_player)
        FirstGroup.objects.create(room_instance=murr_game_room,
                                  murren_player=murren_player,
                                  group_len=group_len_template[room_type])
        SecondGroup.objects.create(room_instance=murr_game_room,
                                   group_len=group_len_template[room_type])
        return murr_game_room

    @database_sync_to_async
    def get_murr_game_room_data(self, murr_game_room_id):
        murr_game_room = MurrGameRoom.objects.filter(pk=murr_game_room_id).first()
        first_group_data = FirstGroup.objects.filter(room_instance=murr_game_room)
        second_group_data = SecondGroup.objects.filter(room_instance=murr_game_room)
        room_groups = {
            'first_group': {
                'members': []
            },
            'second_group': {
                'members': []
            }
        }

        room_groups['first_group']['group_len'] = first_group_data.first().group_len
        room_groups['first_group']['group_id'] = first_group_data.first().pk
        for murren in first_group_data:
            if murren.murren_player:
                room_groups['first_group']['members'].append({
                    'murren_id': murren.murren_player.id,
                    'murren_name': murren.murren_player.username,
                    'murren_avatar': MurrenSerializer.get_avatar(None, murren.murren_player),
                })

        room_groups['second_group']['group_len'] = second_group_data.first().group_len
        room_groups['second_group']['group_id'] = second_group_data.first().pk
        for murren in second_group_data:
            if murren.murren_player:
                room_groups['second_group']['members'].append({
                    'murren_id': murren.murren_player.id,
                    'murren_name': murren.murren_player.username,
                    'murren_avatar': MurrenSerializer.get_avatar(None, murren.murren_player),
                })
        data = {'room_id': murr_game_room.id, 'room_name': murr_game_room.room_name,
                'room_link': murr_game_room.link, 'room_groups': room_groups}

        if room_groups['first_group']['group_len'] == len(
                room_groups['first_group']['members']) and \
                room_groups['second_group']['group_len'] == len(
                room_groups['second_group']['members']):
            murr_game_room.set_available(False)
            data['room_id_for_initiate_battle_scene'] = murr_game_room.id

        return data
