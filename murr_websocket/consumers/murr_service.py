from channels.db import database_sync_to_async
from django.contrib.auth import get_user_model

from murr_notifications.utils import get_notifications, serialize_notification
from murr_websocket.consumers.base import BaseMurrWebSocketConsumer

Murren = get_user_model()


class MurrServiceConsumer(BaseMurrWebSocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope['murr_ws_type'] = 'service'

    async def connect(self):
        await super().connect()
        self.murren = self.scope['murren']
        self.scope['murr_ws_name'] = f'service_{self.scope["murr_ws_name"]}_{self.murren.username}'
        await self.channel_layer.group_add(self.scope["murr_ws_name"], self.channel_name)
        unreaded_notifications = await self.get_unreaded_notifications()
        await self._group_send(unreaded_notifications, gan='unreaded_notifications')

    async def disconnect(self, code):
        await self.channel_layer.group_discard(self.scope['murr_ws_name'], self.channel_name)
        await super().disconnect(code=code)

    async def gan__send_new_notification(self, gan):
        await self._group_send(gan['data'], gan='new_notification')

    @database_sync_to_async
    def get_unreaded_notifications(self):
        notifications = get_notifications(self.murren)
        return [serialize_notification(notify) for notify in notifications]
