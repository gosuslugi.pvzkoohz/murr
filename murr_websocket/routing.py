from django.urls import path

from murr_game.consumers.murr_lobby import MurrGameLobbyConsumer
from murr_game.consumers.murr_room import MurrGameRoomConsumer
from murr_websocket.consumers.murr_chat import MurrChatConsumer
from murr_websocket.consumers.murr_service import MurrServiceConsumer


websocket_urls = [
    path('ws/murr_chat/<str:murr_ws_name>/', MurrChatConsumer),
    path('ws/service/<str:murr_ws_name>/', MurrServiceConsumer),
    path('ws/murr_battle_room/<str:murr_ws_name>/', MurrGameLobbyConsumer),
    path('ws/murr_game_room/<str:room_name>/', MurrGameRoomConsumer)
]
