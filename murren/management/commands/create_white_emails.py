from django.core.management import BaseCommand

from murren.models import TrustedRegistrationEmail


WHITE_LIST_EMAILS = [
  "gmail.com",
  "yandex.ru",
  "yandex.kz",
  "yandex.ua",
  "mail.ru",
  "ya.ru"
]


class Command(BaseCommand):
    help = 'Сгенерировать начальный список почтовых сервисов доступных для  регистрации'

    def handle(self, *args, **options):
        clear = options.get('clear')

        if clear:
            TrustedRegistrationEmail.objects.all().delete()

        if not TrustedRegistrationEmail.objects.count():
            TrustedRegistrationEmail.objects.bulk_create(
                [TrustedRegistrationEmail(service_domain=service) for service in WHITE_LIST_EMAILS]
            )
            self.stdout.write(self.style.SUCCESS('Список белых почтовых сервисов успешно создан'))
        else:
            self.stdout.write(self.style.WARNING(
              'В базе уже существуют почтовые сервисы, чтобы создать начальный список, необходимо очистить все записи. '
              'Для этого воспользуйтесь флагом --clear при запуске команды. '
              'python manage.py create_white_emails --clear'
          ))

    def add_arguments(self, parser):
        parser.add_argument(
          '-c', '--clear', action='store_const', const=True,
          help="Удаляет все записи из таблицы и создает начальный список сервисов"
        )
