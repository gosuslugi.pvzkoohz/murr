import { isJson } from "../../../utils/helpres.js";

import actions from "./actions.js";
import mutations from "./mutations.js";

export const defaultMurrCardData = {
  title: "",
  content: "",
  tags: [],
  category: "",
};

export default {
  state: {
    murrList: [],
    murrListCount: 0,
    murr: null,
    murrDataLocalStorage: JSON.stringify(defaultMurrCardData),
    murrCategories: [],
  },
  getters: {
    murrList: (state) => state.murrList,
    murr: ({ murr }) => {
      if (!murr) {
        return murr;
      }
      let content = murr.content;

      if (isJson(murr.content)) {
        content = JSON.parse(murr.content).blocks;
      }

      return {
        ...murr,
        content,
      };
    },
    murrDataLocalStorage: (state) => {
      if (state.murrDataLocalStorage) {
        return JSON.parse(state.murrDataLocalStorage);
      }

      return defaultMurrCardData;
    },
  },
  actions,
  mutations,
};
