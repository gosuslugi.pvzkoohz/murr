import {
  STATUS_200_OK,
  STATUS_204_NO_CONTENT,
  STATUS_401_UNAUTHORIZED,
} from "../../../utils/http_response_status.js";
import {
  fetchComments,
  addComment,
  deleteComment,
  likeComment,
  unlikeComment,
} from "./api.js";
import {
  ACTIONS_LIKE,
  ACTIONS_DISLIKE,
  ACTIONS_FETCH_COMMENTS,
  ACTIONS_FETCH_NEXT_PAGE_COMMENTS,
  ACTIONS_ADD_COMMENT,
  ACTIONS_REPLY_COMMENT,
  ACTIONS_DELETE_COMMENT,
  MUTATIONS_SET_COMMENT,
  MUTATIONS_ADD_COMMENT,
  MUTATIONS_APPEND_COMMENT,
  MUTATIONS_SET_HAS_NEXT_PAGE,
  MUTATIONS_SET_CURRENT_PAGE,
} from "./type.js";
import { MURR_CARDS_UPDATE_COMMENT_COUNT } from "../../murr_card/store/type.js";

export default {
  async [ACTIONS_FETCH_COMMENTS](context, { murrId }) {
    const { status, data } = await fetchComments(
      murrId,
      context.state.commentsCurrentPage
    );

    if (status !== STATUS_200_OK) {
      await context.dispatch("popUpMessage", {
        message: "Произошла ошибка при загрузке комментариев!",
        customClass: "element-ui-message__error",
        type: "error",
      });
      return null;
    }

    const hasNextPage = data.next !== null;

    context.commit(MUTATIONS_SET_COMMENT, data.results);
    context.commit(MUTATIONS_SET_HAS_NEXT_PAGE, hasNextPage);

    if (hasNextPage) {
      const nextPage = 2;
      context.commit(MUTATIONS_SET_CURRENT_PAGE, nextPage);
    }

    return data;
  },

  async [ACTIONS_FETCH_NEXT_PAGE_COMMENTS](context, { murrId }) {
    const { status, data } = await fetchComments(
      murrId,
      context.state.commentsCurrentPage
    );

    if (status !== STATUS_200_OK) {
      await context.dispatch("popUpMessage", {
        message: "Произошла ошибка при загрузке комментариев!",
        customClass: "element-ui-message__error",
        type: "error",
      });
      return null;
    }

    const hasNextPage = data.next !== null;

    context.commit(MUTATIONS_APPEND_COMMENT, data.results);
    context.commit(MUTATIONS_SET_HAS_NEXT_PAGE, hasNextPage);

    if (hasNextPage) {
      const nextPage = context.state.commentsCurrentPage + 1;
      context.commit(MUTATIONS_SET_CURRENT_PAGE, nextPage);
    }

    return data;
  },

  async [ACTIONS_ADD_COMMENT](context, payload) {
    const currentMurrCard = context.getters.murr;

    const { status, data } = await addComment({
      recaptchaToken: payload.recaptchaToken,
      murr: currentMurrCard.id,
      text: payload.text,
    });

    if (status === STATUS_401_UNAUTHORIZED) {
      await context.dispatch("popUpMessage", {
        message: "Для создания комментариев требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
      return null;
    }

    context.commit(MUTATIONS_ADD_COMMENT, data);
    await context.dispatch(MURR_CARDS_UPDATE_COMMENT_COUNT);

    return data;
  },

  async [ACTIONS_REPLY_COMMENT](context, payload) {
    const currentMurrCard = context.getters.murr;

    const { success, status, data } = await addComment({
      recaptchaToken: payload.recaptchaToken,
      murr: currentMurrCard.id,
      text: payload.text,
      parent: payload.parent,
    });

    if (status === STATUS_401_UNAUTHORIZED) {
      await context.dispatch("popUpMessage", {
        message: "Для создания комментариев требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
      return null;
    }

    if (success) {
      await context.dispatch(MURR_CARDS_UPDATE_COMMENT_COUNT);
      return data;
    }

    return null;
  },

  async [ACTIONS_DELETE_COMMENT](_, commentId) {
    const { status } = await deleteComment(commentId);

    if (status === STATUS_204_NO_CONTENT) {
      return { success: true, message: "Коммент успешно удален!" };
    }

    return null;
  },

  async [ACTIONS_LIKE]({ dispatch }, payload) {
    const { success, status, data } = await likeComment(payload.commentId);

    if (success) {
      return data;
    }

    if (status === STATUS_401_UNAUTHORIZED) {
      dispatch("popUpMessage", {
        message: "Требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
    }

    return null;
  },

  async [ACTIONS_DISLIKE]({ dispatch }, payload) {
    const { success, status, data } = await unlikeComment(payload.commentId);

    if (success) {
      return data;
    }

    if (status === STATUS_401_UNAUTHORIZED) {
      dispatch("popUpMessage", {
        message: "Требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
    }

    return null;
  },
};
