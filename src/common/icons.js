import Vue from "vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { faSmile } from "@fortawesome/free-regular-svg-icons";

library.add(faSmile);

Vue.component("font-awesome-icon", FontAwesomeIcon);
